<?php

class Games_model extends CI_Model {
	
	public function __construct() {
		// Call the CI_Model constructor
		parent::__construct();
	}
	/**
	 * Retorna tots els jocs de la base de dades.
	 * 
	 * @return array Vector amb tots els jocs de la base de dades.
	 */
	public function get_games($num)
	{
		$catalog = array();
		//Si el parámetre no es 0, agafa el nombre de id's com el valor del paràmetre; si no, tots.
		$limit = ($num == 0)? '' : 'LIMIT '.$num;

		$resultat = $this->db->query('SELECT id FROM Cataleg_jocs WHERE visible=1 ORDER BY id DESC ' . $limit . ';');

		foreach ($resultat->result_array() as $row){
			$array_plataforma = array();
			$array_genere = array();
			$id_actual = $row['id'];

			$consulta_cataleg = $this->db->query('SELECT titol, mes_any FROM Cataleg_jocs WHERE id='.$id_actual.';');

			$consulta_id_plataformes = $this->db->query('SELECT id_plataforma FROM Joc_Plataformes WHERE id_joc='.$id_actual.';');

			/*Agafa les plataformes del joc (pot tindre'n més d'una)*/
			foreach ($consulta_id_plataformes->result_array() as $identificador){
				$id_plataforma = $identificador['id_plataforma'];

				$consulta_plataformes = $this->db->query('SELECT nom, url_imatge FROM Plataformes WHERE id='.$id_plataforma.';');

				foreach ($consulta_plataformes->result_array() as $plataformes){
					$array_plataforma[] = array('nom' => $plataformes['nom'], 'url_imatge' => $plataformes['url_imatge']);
				}
			}

			$consulta_id_generes = $this->db->query('SELECT id_genere FROM Joc_Generes WHERE id_joc='.$id_actual.';');

			/*Agafa els géneres del joc (pot tindre'n més d'un)*/
			foreach ($consulta_id_generes->result_array() as $identificador){

				$consulta_generes = $this->db->query('SELECT nom FROM Generes WHERE id='.$identificador['id_genere'].';');

				foreach ($consulta_generes->result_array() as $genere){
					$array_genere[] = array('nom' => $genere['nom']);
				}
			}
			// Selecciona la portada (només una per joc)
			$imatge_portada = $this->db->query('SELECT url_imatge FROM Imatges WHERE id_joc='.$id_actual.' AND tipus="portada";');
			$imatge = $imatge_portada->row_array();

			$cataleg = $consulta_cataleg->row_array();
			
			$catalog[] = array(
					'id'		=> $id_actual,
					'info'		=> $cataleg,
					'image'		=> $imatge,
					'genres'	=> $array_genere,
					'platforms'	=> $array_plataforma
				);
		}

		return $catalog;
	}
	/**
	 * [get_game description]
	 * @param  int $id El camp ID de la taula Cataleg_jocs
	 * @return mixed     False si no troba el joc, un array amb la info d'aquest si sí ho fa.
	 */
	public function get_game($id,$llenguatge)
	{
		$catalog = $this->db->query('SELECT id FROM Cataleg_jocs WHERE visible=1;');

		foreach ($catalog->result_array() as $row){
			if ($id == $row['id']) {
				$sql_info = $this->db->query('SELECT titol, mes_any, url_html5, url_descarga FROM Cataleg_jocs WHERE visible=1 AND id='.$id.';');
				$sql_rel_genres = $this->db->query('SELECT id_genere FROM Joc_Generes WHERE id_joc='.$id.';');
				$array_genres = array();
				$sql_rel_platforms = $this->db->query('SELECT id_plataforma FROM Joc_Plataformes WHERE id_joc='.$id.';');
				$array_platforms = array();
				$array_images = array();

				foreach ($sql_rel_genres->result_array() as $relation){
					$sql_genres = $this->db->query('SELECT nom FROM Generes WHERE id='.$relation['id_genere'].';');

					foreach ($sql_genres->result_array() as $genre){
						$array_genres[] = array('nom' => $genre['nom']);
					}
				}

				foreach ($sql_rel_platforms->result_array() as $relation){
					$sql_platforms = $this->db->query('SELECT nom, url_imatge FROM Plataformes WHERE id='.$relation['id_plataforma'].';');

					foreach ($sql_platforms->result_array() as $platform){
						$array_platforms[] = array('nom' => $platform['nom'], 'url_imatge' => $platform['url_imatge']);
					}
				}

				$sql_images = $this->db->query('SELECT url_imatge FROM Imatges WHERE tipus="slider" AND id_joc='.$id.';');

				foreach ($sql_images->result_array() as $image){
					$array_images[] = $image['url_imatge'];
				}

				//Selecciona descripcio del joc segons el llenguatge
				$descripcio_joc = $this->db->query('SELECT descripcions_'.$llenguatge.' FROM Descripcions WHERE id_joc='.$id.';');
				$descripcio = $descripcio_joc->row_array();

				$cataleg = $sql_info->row_array();
				$cataleg['descripcio'] = $descripcio['descripcions_'.$llenguatge];

				$game = array(
					'id'		=> $id,
					'info'		=> $cataleg,
					'genres'	=> $array_genres,
					'images'	=> $array_images,
					'platforms'	=> $array_platforms
				);

				return $game;
			}
		}

		return false;
	}
	
}
?>