<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('language') == 'en') {
			$data["title"] = "Contact";
		} elseif ($this->session->userdata('language') == 'es') {
			$data["title"] = "Contacto";
		} else {
			$data["title"] = "Contacte";
		}

		$data["language"] = $this->session->userdata('language');

		$this->session->set_userdata('last_site', 'contact');

		$this->load->view('contact_view', $data);
	}

	public function send_mail() {
		$this->email->from($_POST["in_email"], $_POST["in_subject"]);
		$this->email->to('rickymns@gmail.com');
		$this->email->to('a.gus493@gmail.com');
		
		$this->email->subject($_POST["in_subject"]);
		$this->email->message($_POST["in_body"]);

		$this->email->send();
		
		if ($this->session->userdata('language') == 'en') {
			redirect('/contact');
		} elseif ($this->session->userdata('language') == 'es') {
			redirect('/contacto');
		} else {
			redirect('/contacte');
		}
	}
}

/* End of file contact.php */
/* Location: ./application/controllers/contact.php */