<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog extends CI_Controller {

	public function index() {
		if ($this->session->userdata('language') == 'en') {
			$data["title"] = "Catalog";
		} elseif ($this->session->userdata('language') == 'es') {
			$data["title"] = "Catálogo";
		} else {
			$data["title"] = "Catàleg";
		}
		if ($this->session->userdata('language') == 'en') {
			$data["title_section"] = "Catalog";
		} elseif ($this->session->userdata('language') == 'es') {
			$data["title_section"] = "Catalogo";
		} else {
			$data["title_section"] = "Cataleg";
		}

		$data["language"] = ($this->session->userdata('language'))? $this->session->userdata('language') : 'cat';

		$catalog = $this->games_model->get_games(0);
		$data["catalog"] = $catalog;

		$this->session->set_userdata('last_site', 'catalog');

		$this->load->view('catalog_view', $data);
	}

	public function game($id) {
		$language = $this->session->userdata('language');
		if ($language == false) {
			$language = "cat";
		}
		$game = $this->games_model->get_game($id,$language);

		if ($game) {
			if ($this->session->userdata('language') == 'en') {
				$data["title"] = "Game";
			} elseif ($this->session->userdata('language') == 'es') {
				$data["title"] = "Juego";
			} else {
				$data["title"] = "Joc";
			}

			$data["language"] = ($this->session->userdata('language'))? $this->session->userdata('language') : 'cat';
			
			$data["game"] = $game;

			$this->session->set_userdata('last_site', 'catalog');

			$this->load->view('game_view', $data);
		} else {
			echo "error, no s'ha trobat el joc.";
		}
	}
}

/* End of file catalog.php */
/* Location: ./application/controllers/catalog.php */