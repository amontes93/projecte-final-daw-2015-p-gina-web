<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('language') == 'en') {
			$data["title"] = "Home";
		} elseif ($this->session->userdata('language') == 'es') {
			$data["title"] = "Inicio";
		} else {
			$data["title"] = "Inici";
		}
		$data["language"] = ($this->session->userdata('language'))? $this->session->userdata('language') : 'cat';

		$catalog = $this->games_model->get_games(3);
		$data["catalog"] = $catalog;

		$this->session->set_userdata('last_site', 'home');

		$this->load->view('home_view', $data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */