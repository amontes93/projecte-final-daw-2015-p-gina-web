<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language extends CI_Controller {
	public function index() {

	}

	public function change($language) {
		switch ($language) {
			case 0: 
				$this->session->set_userdata('language', 'en');
				break;
			case 1: 
				$this->session->set_userdata('language', 'es');
				break;
			default:
				$this->session->set_userdata('language', 'cat');
				break;
		}

		$last_site = ($this->session->userdata('last_site')) ? $this->session->userdata('last_site') : 'home';

		switch ($last_site) {
			case 'home' :
				if ($language == 0) {
					redirect('/home');
				} elseif ($language == 1) {
					redirect('/inicio');
				} else {
					redirect('/inici');
				}
				break;
			case 'info' :
				if ($language == 0) {
					redirect('/about');
				} elseif ($language == 1) {
					redirect('/informacion');
				} else {
					redirect('/informacio');
				}
				break;
			case 'catalog' :
				if ($language == 0) {
					redirect('/catalog');
				} elseif ($language == 1) {
					redirect('/catalogo');
				} else {
					redirect('/cataleg');
				}
				break;
			case 'contact' :
				if ($language == 0) {
					redirect('/contact');
				} elseif ($language == 1) {
					redirect('/contacto');
				} else {
					redirect('/contacte');
				}
				break;
			default :
				redirect('/inici');
				break;
		}
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */