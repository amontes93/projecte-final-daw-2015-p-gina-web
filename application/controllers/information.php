<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Information extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('language') == 'en') {
			$data["title"] = "About Us";
		} elseif ($this->session->userdata('language') == 'es') {
			$data["title"] = "Información";
		} else {
			$data["title"] = "Informació";
		}

		if ($this->session->userdata('language') == 'en') {
			$data["title_section"] = "About Us";
		} elseif ($this->session->userdata('language') == 'es') {
			$data["title_section"] = "Informacion";
		} else {
			$data["title_section"] = "Informacio";
		}

		$data["language"] = ($this->session->userdata('language'))? $this->session->userdata('language') : 'cat';

		$this->session->set_userdata('last_site', 'info');

		$this->load->view('info_view', $data);
	}

	public function employee($id) {
		if ($this->session->userdata('language') == 'en') {
			$data["title"] = "Employee";
		} elseif ($this->session->userdata('language') == 'es') {
			$data["title"] = "Empleado";
		} else {
			$data["title"] = "Empleat";
		}
		
		$data["id"] = "$id";

		$data["language"] = ($this->session->userdata('language'))? $this->session->userdata('language') : 'cat';

		$this->session->set_userdata('last_site', 'info');

		$this->load->view('employee_view', $data);
	}
}

/* End of file information.php */
/* Location: ./application/controllers/information.php */