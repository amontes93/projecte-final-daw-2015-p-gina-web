<?php
$this->load->view("includes/header");
$this->load->view("includes/menu");
?>
<div id="body">
	<div class="content first">
		<div class="section-contact">
			<img src="<?=public_url()?>img/img.png" alt="img_contact" style="width: 100%;">
		</div>
		<div class="section">
			<div class="four-parts resp-fullwidth">
				<div class="sub-section right">
					<p class="contact-data">C/Falsa, 123</p>
					<p class="contact-data">Terrassa, Barcelona</p>
					<p class="contact-data">08221</p>
					<p class="contact-data">Spain</p>
				</div>
			</div>
			<div class="eight-parts last-column resp-fullwidth">
				<h1 class="nashville"><?=$title?></h1>
				<form method="POST" name="message-contact" action="">
				<div class="container-contact">
					<div class="field field--half">
						<input class="field__input input input--text input--required" id="1" type="text" required/>
						<label class="field__label label--required" for="1">El teu Correu.</label>
					</div>
					<div class="field field--half">
						<input class="field__input input input--text input--required" id="1" type="text" required/>
						<label class="field__label label--required" for="1">L'Assumpte.</label>
					</div>
					<div class="field field--block">
						<textarea class="field__input input input--text input--required" id="1" type="text" required></textarea>
						<label class="field__label label--required" for="1">El cos.</label>
					</div>
					<div class="field field--block form-action">
						<div class="field field--quarter">
							<span class="captcha-label re">Ree</span>
							<span class="captcha-label rand1"></span>
							<span class="captcha-label plus">+</span>
							<span class="captcha-label rand2"></span>
							<span class="captcha-label equal">=</span>
						</div>
						<div class="field field--three-quarter">
							<input class="field__input input input--text input--required" type="text" id="total" autocomplete="off" />
							<label class="field__label label--required" for="1">Resposta</label>
						</div>
					</div>
					<div class="field field--block form-action">
						<button id="submit" type="submit" class="btn btn--action">Envia &rarr;</button>
					</div>
				</div>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<?php
$this->load->view("includes/footer");
?>