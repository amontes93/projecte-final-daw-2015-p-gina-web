<?php
$this->load->view("includes/header");
$this->load->view("includes/menu");
?>
<div id="body">
	<div class="content">
		<div class="section first">
			<div id="slider1_container"	>
				<div id="slides" u="slides">
					<?php
						for ($i = 0; $i < count($game["images"]); $i++) { ?>
							<div><img u="image" src="<?=public_url()?>img<?=$game["images"][$i]?>" alt="slider-<?=$i?>" style="width: 100%;"></div>
					<?php	}	?>
				</div>
				<link rel="stylesheet" type="text/css" href="<?=public_url()?>css/jssor_style.css">
				<div u="navigator" class="jssorb03" style="bottom: -295px; right: 6px;">
		            <div u="prototype"><div u="numbertemplate"></div></div>
		        </div>
			</div>
		</div>
		<div class="section">
			<div class="four-parts">
				<?php
					if ($game["info"]["url_html5"] == ("" || null || " ")) {
				?>
						<div class="sub-section">
							<button style="width: 90%; height: 100px;" onclick="alert('<?=$game['info']['url_html5']?>')">Juga-hi!</button>
						</div>
				<?php
					}
				?>
				<?php
					if ($game["info"]["url_descarga"] == ("" || null || " ")) {
				?>
						<div class="sub-section">
							<button style="width: 90%; height: 100px;" onclick="alert('<?=$game['info']['url_descarga']?>')">Descarrega'l!</button>
						</div>
				<?php
					}
				?>
				<div class="sub-section">
					<h4 class="game-desc-subtitle">Plataformes disponibles</h4>
					<?php
						foreach ($game['platforms'] as $platform) {
					?>
							<i class="fa <?=$platform['url_imatge']?>" alt="<?=$platform['nom']?>"></i>
					<?php
						}
					?>
				</div>
				<div class="sub-section">
					<h4 class="game-desc-subtitle">Any de sortida</h4>
					<p><?=$game['info']['mes_any']?></p>
				</div>
				<div class="sub-section">
					<h4 class="game-desc-subtitle">Génere</h4>
					<p>
					<?php
						foreach ($game['genres'] as $genre) {
					?>
							<span><?=$genre['nom']?>, </span>
					<?php
						}
					?>
					</p>
				</div>
			</div>
			<div class="eight-parts last-column">
				<h1 class="nashville"><?=$game["info"]["titol"]?></h1>
				<p><?=$game['info']['descripcio']?></p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<?php
$this->load->view("includes/footer");
?>