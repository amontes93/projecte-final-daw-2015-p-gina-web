<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="<?=public_url()?>css/main_style.css">
	<link rel="stylesheet" type="text/css" href="<?=public_url()?>css/home_style.css">
	<link rel="stylesheet" type="text/css" href="<?=public_url()?>css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?=public_url()?>css/contact_style.css">
	<link rel="stylesheet" type="text/css" href="<?=public_url()?>css/menu_style.css">
	<link rel="stylesheet" href="<?=public_url()?>font-awesome/css/font-awesome.min.css">
	
	<script src="<?=public_url()?>lib/jquery-2.1.3.min.js"></script>
	<script src="<?=public_url()?>js/captcha.js"></script>
	<title><?=$title?></title>
</head>
<body>
	<div class="wrapper">