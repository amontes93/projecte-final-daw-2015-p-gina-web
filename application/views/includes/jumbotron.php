

<div class="jumbotron">
	<div class="jumbo-social-container">
		<a href="http://www.twitter.com/"><i class="social-jumbo fa fa-twitter"></i></a>
		<a href="http://www.facebook.com/"><i class="social-jumbo fa fa-facebook"></i></a>
		<a href="http://www.youtube.com/"><i class="social-jumbo fa fa-youtube"></i></a>
		<a href="http://plus.google.com/"><i class="social-jumbo fa fa-google-plus"></i></a>
		<span class="float-right">
			<a href="<?=base_url()?>index.php/lang/2">
				<img class="lang-flag" src="<?=public_url()?>img/catalan_flag.png" alt="Catalan Flag">
			</a>
			<a href="<?=base_url()?>index.php/lang/1">
				<img class="lang-flag" src="<?=public_url()?>img/spanish_flag.png" alt="Spansih Flag">
			</a>
			<a href="<?=base_url()?>index.php/lang/0">
				<img class="lang-flag" src="<?=public_url()?>img/uk_flag.png" alt="United Kingdom Flag">
			</a>

		</span>
	</div>
	<div id="jumbo-logo-container" class="center">
		<img src="<?=public_url()?>img/img.png" alt="logo" class="logo">
	</div>

	<div id="last-games-container" class="twelve-parts">
		<div id="last-games">
			<?php
				foreach ($catalog as $game) {
			?>
					<div class="four-parts">
						<a href="<?=base_url()?>index.php/joc/<?=$game["id"]?>">
							<img src="<?=public_url()?>img/<?=$game["image"]["url_imatge"]?>" alt="<?=$game["info"]["titol"]?>">
						</a>
					</div>
			<?php
				}
			?>	
		</div>
	</div>
</div>