<header>
	<div id="anchor">
		<div class="two-parts">
				<a href="<?=base_url()?>index.php"><img style="height: 50px; padding: 25px 0 0 25px;" src="<?=public_url()?>img/img.png"/></a>
		</div>
		<div class="nine-parts">
			<nav class="menu">
				<ul>
					<?php
						if($language) {
							switch ($language) {
								case 'en':
									?>
										<li class="nashville three-parts"><a href="<?=base_url()?>index.php/home" title="home" alt="home">HOME</a></li>
										<li class="nashville three-parts"><a href="<?=base_url()?>index.php/about" title="about us" alt="about us">About Us</a></li>
										<li class="nashville three-parts"><a href="<?=base_url()?>index.php/catalog" title="catalog" alt="catalog">Catalog</a></li>
										<li class="nashville three-parts last-column"><a href="<?=base_url()?>index.php/contact" title="contact" alt="contact">Contact</a></li>
									<?php
									break;
								case 'es':
									?>
										<li class="nashville three-parts"><a href="<?=base_url()?>index.php/inicio" title="inicio" alt="inicio">Inicio</a></li>
										<li class="nashville three-parts"><a href="<?=base_url()?>index.php/informacion" title="informacion" alt="informacion">Informacion</a></li>
										<li class="nashville three-parts"><a href="<?=base_url()?>index.php/catalogo" title="catalogo" alt="catalogo">Catalogo</a></li>
										<li class="nashville three-parts last-column"><a href="<?=base_url()?>index.php/contacto" title="contacto" alt="contacto">Contacto</a></li>
									<?php
									break;
								case 'cat':
									?>
										<li class="nashville three-parts"><a href="<?=base_url()?>index.php/inici" title="inici" alt="inici">Inici</a></li>
										<li class="nashville three-parts"><a href="<?=base_url()?>index.php/informacio" title="informacio" alt="informacio">Informacio</a></li>
										<li class="nashville three-parts"><a href="<?=base_url()?>index.php/cataleg" title="cataleg" alt="cataleg">Cataleg</a></li>
										<li class="nashville three-parts last-column"><a href="<?=base_url()?>index.php/contacte" title="contacte" alt="contacte">Contacte</a></li>
									<?php
									break;
							}
						} else {
					?>
							<li class="nashville three-parts"><a href="<?=base_url()?>index.php/inici" title="inici" alt="inici">Inici</a></li>
							<li class="nashville three-parts"><a href="<?=base_url()?>index.php/informacio" title="informacio" alt="informacio">Informacio</a></li>
							<li class="nashville three-parts"><a href="<?=base_url()?>index.php/cataleg" title="cataleg" alt="cataleg">Cataleg</a></li>
							<li class="nashville three-parts last-column"><a href="<?=base_url()?>index.php/contacte" title="contacte" alt="contacte">Contacte</a></li>
					<?php
						}
					?>
					<div class="clearfix"></div>
				</ul>
			</nav>
		</div>
		<div class="one-part last-column social-container">
			<div class="six-parts">
				<a href="http://www.twitter.com/"><i class="social-menu fa fa-twitter"></i></a>
			</div>
			<div class="six-parts last-column">
				<a href="<?=base_url()?>index.php/lang/0"><img class="lang-flag" src="<?=public_url()?>img/uk_flag.png" alt="United Kingdom Flag"></a>
			</div>
			<div class="clearfix"></div>
			<div class="six-parts">
				<a href="http://www.facebook.com/"><i class="social-menu fa fa-facebook"></i></a>
			</div>
			<div class="six-parts last-column">
				<a href="<?=base_url()?>index.php/lang/1"><img class="lang-flag" src="<?=public_url()?>img/spanish_flag.png" alt="Spansih Flag"></a>

			</div>
			<div class="clearfix"></div>
			<div class="six-parts">
				<a href="http://www.youtube.com/"><i class="social-menu fa fa-youtube"></i></a>
			</div>
			<div class="six-parts last-column">
				<a href="<?=base_url()?>index.php/lang/2"><img class="lang-flag" src="<?=public_url()?>img/catalan_flag.png" alt="Catalan Flag"></a>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div id="menu-tablet">
		<div><a href="<?=base_url()?>index.php"><img style="width: 100%; padding: 25px 0 0 25px;" src="<?=public_url()?>img/img.png"/></a></div>
		<div id="responsive-social">
			<div class="six-parts resp-fullwidth center">
				<a href="http://www.twitter.com/"><i class="social-menu fa fa-twitter"></i></a>
				<a href="http://www.facebook.com/"><i class="social-menu fa fa-facebook"></i></a>
				<a href="http://www.youtube.com/"><i class="social-menu fa fa-youtube"></i></a>
				<a href="http://plus.google.com/"><i class="social-menu fa fa-google-plus"></i></a>
			</div>
			<div class="six-parts resp-fullwidth last-column center">
				<a href="<?=base_url()?>index.php/lang/0"><img class="lang-flag" src="<?=public_url()?>img/uk_flag.png" alt="United Kingdom Flag"></a>
				<a href="<?=base_url()?>index.php/lang/1"><img class="lang-flag" src="<?=public_url()?>img/spanish_flag.png" alt="Spanish Flag"></a>
				<a href="<?=base_url()?>index.php/lang/2"><img class="lang-flag" src="<?=public_url()?>img/catalan_flag.png" alt="Catalan Flag"></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="outer-menu" class="outer-menu">
		<input id="checkbox_menu" class="checkbox-toggle" type="checkbox" />
		<div class="hamburger">
	    	<div></div>
		</div>
	  	<div id="menu_mobile" class="menu-mobil-ocult">
		    <div>
		    	<div>
		        <ul>
					<?php
						if($language) {
							switch ($language) {
								case 'en':
									?>
										<li class="nashville"><a href="<?=base_url()?>index.php/home" title="home" alt="home">HOME</a></li>
										<li class="nashville"><a href="<?=base_url()?>index.php/about" title="about us" alt="about us">About Us</a></li>
										<li class="nashville"><a href="<?=base_url()?>index.php/catalog" title="catalog" alt="catalog">Catalog</a></li>
										<li class="nashville"><a href="<?=base_url()?>index.php/contact" title="contact" alt="contact">Contact</a></li>
									<?php
									break;
								case 'es':
									?>
										<li class="nashville"><a href="<?=base_url()?>index.php/inicio" title="inicio" alt="inicio">Inicio</a></li>
										<li class="nashville"><a href="<?=base_url()?>index.php/informacion" title="informacion" alt="informacion">Informacion</a></li>
										<li class="nashville"><a href="<?=base_url()?>index.php/catalogo" title="catalogo" alt="catalogo">Catalogo</a></li>
										<li class="nashville"><a href="<?=base_url()?>index.php/contacto" title="contacto" alt="contacto">Contacto</a></li>
									<?php
									break;
								case 'cat':
									?>
										<li class="nashville"><a href="<?=base_url()?>index.php/inici" title="inici" alt="inici">Inici</a></li>
										<li class="nashville"><a href="<?=base_url()?>index.php/informacio" title="informacio" alt="informacio">Informacio</a></li>
										<li class="nashville"><a href="<?=base_url()?>index.php/cataleg" title="cataleg" alt="cataleg">Cataleg</a></li>
										<li class="nashville"><a href="<?=base_url()?>index.php/contacte" title="contacte" alt="contacte">Contacte</a></li>
									<?php
									break;
							}
						} else {
					?>
							<li class="nashville"><a href="<?=base_url()?>index.php/inici" title="inici" alt="inici">Inici</a></li>
							<li class="nashville"><a href="<?=base_url()?>index.php/informacio" title="informacio" alt="informacio">Informacio</a></li>
							<li class="nashville"><a href="<?=base_url()?>index.php/cataleg" title="cataleg" alt="cataleg">Cataleg</a></li>
							<li class="nashville"><a href="<?=base_url()?>index.php/contacte" title="contacte" alt="contacte">Contacte</a></li>
					<?php
						}
					?>
					<div class="clearfix"></div>
				</ul>
		    	</div>
		    </div>
		</div>
	</div>
	<script>
		$("#checkbox_menu").click(function(){
			if($(this).prop("checked")){
				document.getElementById('menu_mobile').setAttribute('class','menu-mobil');
			} else {
				document.getElementById('menu_mobile').setAttribute('class','menu-mobil-ocult');
			}
		});
	</script>
</header>
	<div id="menu-fixer">.</div>