<?php
$this->load->view("includes/header");
$this->load->view("includes/menu");
?>
<div id="body">
	<div class="content">
		<div class="section first">
			<h1 class="nashville">Cataleg</h1>
			<div class="section">
				<div class="row">
					<?php
						$total=0;
						$count=0;
						foreach ($catalog as $game) {
							$total++;
							$count++;
							$final = false;
							if ($count % 3 == 0) {
								$final = true;
								$count = 0;
							}
							if (!$final) {
								echo '<div class="four-parts catalog-view">';
							} else {
								echo '<div class="four-parts last-column catalog-view">';
							}
					?>
								<a href="<?=base_url()?>index.php/joc/<?=$game['id']?>"><img class="game-thumbnail" src="<?=public_url()?>img<?=$game['image']['url_imatge']?>" alt="gnu-cat"/>
								<h2><?=$game['info']['titol']?></h2>
								<div>
									<?php
										foreach ($game['platforms'] as $platform) {
									?>
											<i class="fa <?=$platform['url_imatge']?>"></i>
									<?php
										}
									?>
								</div>
								<p class="center"> <?=$game['info']['mes_any']?> - <?=$game['genres'][0]['nom']?></p>
								</a>
							</div>
							
					<?php 
							if ($final && !($total >= count($catalog))) {
								echo '
									<div class="clearfix"></div>
								</div> <?//row?>
								';
							}
						}
					?>

					<div class="clearfix"></div>
				</div> <?//row?>
				
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<?php
$this->load->view("includes/footer");
?>