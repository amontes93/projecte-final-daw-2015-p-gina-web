<?php
$this->load->view("includes/header");
$this->load->view("includes/jumbotron");
$this->load->view("includes/menu");
?>
<div id="body">
	<div class="content">
		<div class="section">
			<?php
				if (!$language){
					echo "false";
				} else {
					echo $language;
				}
			?>
			<h1 class="nashville">Informacio</h1>
			<p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora odio illum animi esse veritatis? Nihil fugit vero, qui nostrum ipsum laboriosam iusto aliquam voluptates sed suscipit corrupti, ullam, reiciendis. Numquam.</span>
			<span>Consequuntur magni, quam. Iste ipsa enim praesentium nihil neque rem tempora at rerum? Mollitia assumenda velit ad sequi, quo dolor natus commodi ipsum illum, necessitatibus id voluptas consequuntur nostrum tempora.</span>
			<span>Molestiae autem unde perspiciatis maiores, culpa voluptatem sapiente, doloribus, est quos et laborum quisquam in minima magnam ratione cumque ea. Hic enim quaerat, sequi perferendis ea tempora reiciendis reprehenderit architecto.</span></p>
		</div>
		<div class="section">
			<img style="width: 100%" src="<?=public_url()?>img/img.png"/>
		</div>
		<div class="section">
			<h1 class="nashville">Cataleg</h1>
			<p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora odio illum animi esse veritatis? Nihil fugit vero, qui nostrum ipsum laboriosam iusto aliquam voluptates sed suscipit corrupti, ullam, reiciendis. Numquam.</span>
			<span>Consequuntur magni, quam. Iste ipsa enim praesentium nihil neque rem tempora at rerum? Mollitia assumenda velit ad sequi, quo dolor natus commodi ipsum illum, necessitatibus id voluptas consequuntur nostrum tempora.</span>
			<span>Molestiae autem unde perspiciatis maiores, culpa voluptatem sapiente, doloribus, est quos et laborum quisquam in minima magnam ratione cumque ea. Hic enim quaerat, sequi perferendis ea tempora reiciendis reprehenderit architecto.</span></p>
		</div>
		<div class="section">
			<img style="width: 100%" src="<?=public_url()?>img/img.png"/>
		</div>
		<div class="section">
			<h1 class="nashville">Contacte</h1>
			<p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora odio illum animi esse veritatis? Nihil fugit vero, qui nostrum ipsum laboriosam iusto aliquam voluptates sed suscipit corrupti, ullam, reiciendis. Numquam.</span>
			<span>Consequuntur magni, quam. Iste ipsa enim praesentium nihil neque rem tempora at rerum? Mollitia assumenda velit ad sequi, quo dolor natus commodi ipsum illum, necessitatibus id voluptas consequuntur nostrum tempora.</span>
			<span>Molestiae autem unde perspiciatis maiores, culpa voluptatem sapiente, doloribus, est quos et laborum quisquam in minima magnam ratione cumque ea. Hic enim quaerat, sequi perferendis ea tempora reiciendis reprehenderit architecto.</span></p>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<?php
$this->load->view("includes/footer");
?>