var elementPosition = $('#anchor').offset();

$(window).scroll(function(){
	if ($('#anchor').css('display') != 'none') {
		if($(window).scrollTop() > elementPosition.top){
			$('#anchor').css('position','fixed').css('top','0').css('width', '100%').css('z-index', '10');
			$('#menu-fixer').css('display', 'block');
		} else {
			$('#anchor').css('position','static');
			$('#menu-fixer').css('display', 'none');
		}
	}
});