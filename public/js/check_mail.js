var error = false;

function comprobarDatos(){
    error = false;
    
    limpiarFondo();

    var mail = formulario.in_email.value;
    
    checkEmail(mail);
    if (error == false){
        return true;
    } else {
        return false;
    }
}

function checkEmail(valor) {
    var patt = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/;
    if (patt.test(valor) == false){
        error = true;
        document.forms[0].in_email.style.backgroundColor="red";
        document.forms[0].in_email.focus();
    }
}

function limpiarFondo(){
    document.forms[0].in_email.style.backgroundColor="white";
}